const containers = document.getElementsByClassName("clearfix well well-sm");
getData(0);

function getData(i) {
  if (i == containers.length) return;

  const audios = containers[i].getElementsByClassName("small-audio");
  const word_us = audios[0].firstElementChild.src;
  const word_uk = audios[1].firstElementChild.src;
  const example_us = audios[2].firstElementChild.src;
  const example_uk = audios[3].firstElementChild.src;

  // const pTags = containers[i].getElementsByTagName("p");
  const dt = containers[i].getElementsByTagName("dt");
  const word = dt[0].innerText;
  const translation = "";

  const dd = containers[i].getElementsByTagName("dd");
  const definition_en = dd[0].innerText;
  const definition_vi = "";

  const noborder = containers[i].getElementsByClassName("noborder");
  const example_en = noborder[1].innerText;
  const example_vi = "";

  // const spanTags = containers[i].getElementsByTagName("span");
  const spelling = "";

  console.log(
    `${word}\t${spelling}\t${word_us}\t${word_uk}\t${translation}\t${definition_en}\t${definition_vi}\t${example_en}\t${example_vi}\t${example_us}\t${example_uk}`
  );

  fetchFile(word_us, `${word.replace(/ /g, "_").toLowerCase()}_us`);
  setTimeout(function () {
    fetchFile(word_uk, `${word.replace(/ /g, "_").toLowerCase()}_uk`);
    setTimeout(function () {
      fetchFile(
        example_us,
        `${word.replace(/ /g, "_").toLowerCase()}_example_us`
      );
      setTimeout(function () {
        fetchFile(
          example_uk,
          `${word.replace(/ /g, "_").toLowerCase()}_example_uk`
        );
        setTimeout(function () {
          getData(++i);
        }, 1000);
      }, 1000);
    }, 1000);
  }, 1000);
}

function fetchFile(url, name) {
  fetch(url)
    .then((res) => res.blob())
    .then((file) => {
      let tempUrl = URL.createObjectURL(file);
      let aTag = document.createElement("a");
      aTag.href = tempUrl;
      aTag.download = name;
      document.body.appendChild(aTag);
      aTag.click();
      aTag.remove();
      URL.revokeObjectURL(tempUrl);
    })
    .catch(() => {
      alert("Failed to download file!");
    });
}
