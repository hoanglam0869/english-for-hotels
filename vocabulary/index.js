import { hideLoading, showLoading } from "../function/function.js";
import { courses } from "../lesson/courses.js";
import { renderNavigation } from "../nav/controller.js";
import { renderVocabulary } from "./controller.js";

const url = window.location.href;
const id = url.substring(url.length - 1, url.length) * 1;

renderNavigation(id, courses, 0);

let vocabularyArr = [];

const fetchData = async () => {
  showLoading();
  let res = await fetch("./utils/json/vocabulary.json");
  vocabularyArr = await res.json();
  vocabularyArr = vocabularyArr.filter(
    (vocabulary) => vocabulary.lesson_id == id
  );
  renderVocabulary(vocabularyArr);
  hideLoading();
};

fetchData();
