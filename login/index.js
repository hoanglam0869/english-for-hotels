import { hideLoading, showLoading } from "../function/function.js";

window.fbAsyncInit = function () {
  showLoading();
  // FB JavaScript SDK configuration and setup
  FB.init({
    appId: "3855156004771045",
    cookie: true,
    xfbml: true,
    version: "v16.0",
    status: true,
  });

  // Check whether the user already logged in
  FB.getLoginStatus(function (response) {
    console.log(response);
    if (response.status == "connected") {
      // display user data
      //getFBUserData();
      fbLogout();
    } else {
      hideLoading();
    }
  });
};

// Load the JavaScript SDK asynchronously
(function (d, s, id) {
  var js,
    fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
})(document, "script", "facebook-jssdk");

// Facebook login with JavaScript SDK
function fbLogin() {
  FB.login(
    function (response) {
      console.log(response);
      if (response.authResponse) {
        // Get and display the user profile data
        getFBUserData();
      } else {
        console.log("User cancelled login or did not fully authorize.");
      }
    },
    { scope: "email" }
  );
}

// Fetch the user profile data from facebook
function getFBUserData() {
  FB.api(
    "/me",
    {
      locale: "en_US",
      fields: "id, first_name, last_name, email, link, gender, locale, picture",
    },
    function (response) {
      console.log(response);
      window.location.href = "/";
    }
  );
}

// Logout from facebook
function fbLogout() {
  FB.logout(function () {
    console.log("Logged out");
    hideLoading();
  });
}

document.querySelector(".facebooklogin-fb").onclick = () => {
  fbLogin();
};
