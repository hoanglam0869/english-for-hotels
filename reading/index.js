import { hideLoading, showLoading } from "../function/function.js";
import { courses } from "../lesson/courses.js";
import { renderNavigation } from "../nav/controller.js";
import { renderQuestion, renderModal, renderReading } from "./controller.js";

const url = window.location.href;
const id = url.substring(url.length - 1, url.length) * 1;

renderNavigation(id, courses, 1);

let lessonsArr = [];
let readingArr = [];
let vocabularyArr = [];
let questionArr = [];

const fetchData = async () => {
  showLoading();
  let res = await fetch("./utils/json/lessons.json");
  lessonsArr = await res.json();
  res = await fetch("./utils/json/reading.json");
  readingArr = await res.json();
  readingArr = readingArr.filter((reading) => reading.lesson_id == id);
  res = await fetch("./utils/json/vocabulary.json");
  vocabularyArr = await res.json();
  vocabularyArr = vocabularyArr.filter(
    (vocabulary) => vocabulary.lesson_id == id
  );
  res = await fetch("./utils/json/question.json");
  questionArr = await res.json();
  questionArr = questionArr.filter((question) => question.lesson_id == id);
  renderReading(lessonsArr[id - 1].name, readingArr);
  renderQuestion(id, questionArr);
  handleClickAudio();
  hideLoading();
};

fetchData();

window.handleClick = (el) => {
  let word = el.innerText;
  let vocabulary = vocabularyArr.filter((item) => {
    let regex = new RegExp(`^${word}$`, "ig");
    if (regex.test(`${item.word}`)) {
      return true;
    } else if (regex.test(`${item.word}s`)) {
      return true;
    } else if (regex.test(`${item.word}es`)) {
      return true;
    } else if (/[\(\)]/gi.test(item.word)) {
      regex = new RegExp(`\\(*${word}\\)*`, "ig");
      if (regex.test(item.word)) return true;
      else if (regex.test(item.word.replace(")", "s)"))) return true;
    }
  });
  renderModal(true, vocabulary[0]);
};

const handleClickAudio = () => {
  const audioEl = document.querySelector(".audio-player-wrapper audio");
  audioEl.src = lessonsArr[id - 1].reading_audio;
  let audio = new Audio(audioEl.src);
  audio.preload;
  const slider = document.querySelector(".audio-player-slider");
  const timeTxt = document.querySelector(".audio-player-timeline-current");
  audio.onloadedmetadata = () => {
    slider.value = slider.min;
    slider.max = audio.duration;
    timeTxt.innerText = "0:0";
  };
  let isPlaying = false;
  let isDragging = false;
  const playBtn = document.querySelector(".audio-player-button");
  playBtn.onclick = () => {
    if (isPlaying) audio.pause();
    else audio.play();

    playBtn.innerHTML = `<i class="fa fa-${isPlaying ? "play" : "pause"}"></i>`;
    isPlaying = !isPlaying;
  };
  audio.ontimeupdate = () => {
    timeTxt.innerText = formatTime(audio.currentTime);
    if (isDragging) return;
    slider.value = (audio.currentTime * slider.max) / audio.duration;
  };
  slider.oninput = () => {
    isDragging = true;
  };
  slider.onchange = () => {
    isDragging = false;
    audio.currentTime = (slider.value * audio.duration) / slider.max;
  };
  audio.onended = () => {
    playBtn.innerHTML = `<i class="fa fa-play"></i>`;
  };
};

const formatTime = (time) => {
  let minute = Math.floor(time / 60);
  let second = Math.floor(time - minute * 60);
  return `${minute}:${second}`;
};
