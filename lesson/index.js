import { hideLoading, showLoading } from "../function/function.js";
import { renderLesson } from "./controller.js";
import { courses } from "./courses.js";

const url = window.location.href;
const id = url.substring(url.length - 1, url.length);

showLoading();
renderLesson(id, courses);
hideLoading();
