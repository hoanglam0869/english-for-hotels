export const $ = (tagName, className, container) => {
  let self = document.createElement(tagName);
  if (className != "") self.className = className;
  container.appendChild(self);

  self.html = (html) => {
    if (html) {
      self.innerHTML = html;
      return self;
    }
    return self.innerHTML;
  };

  if (tagName != "a") {
    self.text = (txt) => {
      if (txt) {
        self.innerText = txt;
        return self;
      }
      return self.innerText;
    };
  }

  self.attr = (key, value) => {
    if (value) {
      self.setAttribute(key, value);
      return self;
    }
    return self.getAttribute(key);
  };

  return self;
};

export const showLoading = () => {
  const loading = $("div", "loading", document.body);
  $("img", "", loading).attr("src", "../img/loading.gif");
};

export const hideLoading = () => {
  document.querySelector(".loading").remove();
};
