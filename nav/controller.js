import { $ } from "../function/function.js";

export const renderNavigation = async (id, courses, index) => {
  /* document.querySelectorAll(".nav-item").forEach((liTag, i) => {
    if (i == 4) return;
    let name = courses[i].name;
    liTag.innerHTML = `<a href="/${name.replace(
      " ",
      ""
    )}.html?${id}" class="nav-title${index == i ? " nav-active" : ""}">${
      name.charAt(0).toUpperCase() + name.slice(1)
    }</a>`;
  });

  let res = await fetch("./utils/json/lessons.json");
  let lessonsArr = await res.json();
  document.querySelector(".sidebar-menu-dropdown").innerHTML = lessonsArr
    .map((lesson) =>
      lesson.lock
        ? ""
        : `
        <div class="sidebar-menu-dropdown-option">
            <a href="/lesson/unit.html?${lesson.id}" style="text-decoration: none;">Lesson ${lesson.id}</a>
        </div>
    `
    )
    .join("");
  handleClickNavigation(); */
  const navItem = document.querySelectorAll(".nav-item");
  navItem.forEach((item, i) => {
    if (i == 4) return;
    let name = courses[i].name;
    const aTag = $("a", `nav-title${index == i ? " nav-active" : ""}`, item);
    aTag.href = `/${name.replace(" ", "")}.html?${id}`;
    aTag.innerText = name.charAt(0).toUpperCase() + name.slice(1);
  });

  let res = await fetch("./utils/json/lessons.json");
  let lessonsArr = await res.json();
  const dropdown = document.querySelector(".sidebar-menu-dropdown");
  lessonsArr.forEach((lesson) => {
    if (lesson.lock) return;
    const option = $("div", "sidebar-menu-dropdown-option", dropdown);
    const aTag = $("a", "", option);
    aTag.href = `/lesson/unit.html?${lesson.id}`;
    aTag.style = `text-decoration: none;`;
    aTag.innerText = `Lesson ${lesson.id}`;
  });
  handleClickNavigation();
};

export const handleClickNavigation = () => {
  const navFixWidth = document.querySelector(".nav-fix-width");
  const navSidebar = document.querySelector(".nav-sidebar");
  const navMenuDrop = document.querySelector(".nav-menu-drop");
  // show or hide navigation
  document.querySelector(".nav-icon").onclick = () => {
    navFixWidth.classList.toggle("nav-hide-fix-width");
    navSidebar.classList.toggle("nav-hide-sidebar");
    navSidebar.classList.toggle("nav-original-sidebar");
  };
  // show dropdown lesson list
  document.querySelector("#sidebar-menu-drop").onclick = () => {
    navMenuDrop.classList.toggle("sidebar-menu-active");
  };
  // set navigation fixed position when scroll
  window.onscroll = () => {
    if (window.scrollY > 65) {
      navSidebar.classList.add("nav-fixedCss");
    } else {
      navSidebar.classList.remove("nav-fixedCss");
    }
  };
};
