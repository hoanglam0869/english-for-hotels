import { hideLoading, showLoading } from "../function/function.js";
import { courses } from "../lesson/courses.js";
import { localListeningStorage } from "../local-storage/local-storage.js";
import { renderNavigation } from "../nav/controller.js";
import { renderLetters } from "./controller.js";

const url = window.location.href;
const id = url.substring(url.length - 1, url.length) * 1;

renderNavigation(id, courses, 2);

const restart = document.querySelector(".btn-restart");
const status = document.querySelectorAll(".showStatic p");
const progress = document.querySelector(".progress-line-correct");
const soundUS = document.querySelector(".us");
const soundUK = document.querySelector(".uk");
const audioEl = document.querySelectorAll(".audio-container audio");
const input = document.querySelector(".word-input");
const btn = document.querySelector(".btn");

let listening = {};
let vocabularyArr = [];
let currentIndex = 0;
let isSubmit = true;

const length = () => vocabularyArr.length;

const fetchData = async () => {
  showLoading();
  listening = localListeningStorage.get(id);
  if (listening.lessons.length == 0) {
    let res = await fetch("./utils/json/vocabulary.json");
    vocabularyArr = await res.json();
    vocabularyArr = vocabularyArr.filter((vocab) => vocab.lesson_id == id);
    reset();
  } else {
    vocabularyArr = listening.lessons;
    currentIndex = listening.index % length();
    setupListening();
  }
  hideLoading();
};

fetchData();

const reset = () => {
  vocabularyArr.sort(() => Math.random() - 0.5);
  listening.lessons = vocabularyArr;
  // lưu lessons mới vào localStorage, để khi load trang không render lại lessons cũ
  localListeningStorage.set(listening);
  currentIndex = 0;
  setupListening();
};

function setupListening() {
  // status
  let index = listening.index > length() ? length() : listening.index;
  status[0].innerText = `${index} of ${length()}`;
  let correct = listening.total == 0 ? 0 : listening.correct / listening.total;
  status[1].innerText = `${Math.floor(correct * 100)}% correct so far`;
  // progress bar
  progress.style = `width: ${(index * 100) / length()}%;`;
  // audio element
  audioEl[0].src = vocabularyArr[currentIndex].sound_us;
  audioEl[1].src = vocabularyArr[currentIndex].sound_uk;
  // letters
  renderLetters(vocabularyArr[currentIndex]);
  // input
  input.value = "";
  // button
  isSubmit = true;
  btn.innerText = "SUBMIT";
  btn.setAttribute("disabled", "");
}

const playSound = (url) => {
  let audio = new Audio(url);
  audio.preload;
  audio.play();
  input.focus();
};

restart.onclick = () => {
  restart.classList.add("success");
  localListeningStorage.reset(id);
  setTimeout(() => {
    restart.classList.remove("success");
    // lấy object listening sau khi reset
    listening = localListeningStorage.get(id);
    reset();
  }, 1000);
};
soundUS.onclick = () => playSound(audioEl[0].src);
soundUK.onclick = () => playSound(audioEl[1].src);
input.oninput = () => {
  if (input.value == "") {
    btn.setAttribute("disabled", "");
  } else {
    btn.removeAttribute("disabled");
  }
};
btn.onclick = () => {
  if (isSubmit) {
    submit();
    isSubmit = false;
  } else {
    next();
  }
};
input.onkeyup = (e) => {
  if (e.key == "Enter") btn.click();
};

const submit = () => {
  let count = 0;
  let correctWord = vocabularyArr[currentIndex].word.toLowerCase();
  const con = document.querySelectorAll(".letter-container");
  for (let i = 0; i < con.length; i++) {
    const div = con[i].firstChild;
    if (i > input.value.length - 1) {
      div.style = `color: red;`;
    } else if (input.value[i].toLowerCase() == correctWord[i]) {
      div.style = `color: green;`;
      count++;
    } else {
      div.style = `color: red;`;
    }
    div.innerText = correctWord[i];
  }
  if (count == correctWord.length) {
    currentIndex++;
    listening.index = listening.index + 1;
    listening.correct = listening.correct + 1;
  } else {
    let temp = vocabularyArr[currentIndex];
    vocabularyArr.splice(currentIndex, 1);
    vocabularyArr.push(temp);
  }
  listening.total = listening.total + 1;
  localListeningStorage.set(listening);
  btn.innerText = "NEXT";
};

const next = () => {
  if (currentIndex == vocabularyArr.length) {
    reset();
  } else {
    setupListening();
  }
};

status[0].onclick = () => {
  input.value = vocabularyArr[currentIndex].word;
  btn.removeAttribute("disabled");
  setTimeout(() => {
    btn.click();
    setTimeout(() => {
      btn.click();
    }, 1000);
  }, 1000);
};
