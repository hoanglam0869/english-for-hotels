import { $ } from "../function/function.js";

const isLetter = () => {
  let rnd = Math.floor(Math.random() * 2);
  return rnd == 1 ? true : false;
};

export const renderLetters = (vocabulary) => {
  let hintLength = Math.ceil(vocabulary.word.length / 4);
  let count = 0;

  const wordResult = document.querySelector(".word-result");
  wordResult.innerHTML = "";
  for (let i = 0; i < vocabulary.word.length; i++) {
    const letter = vocabulary.word[i];
    const con = $("div", "letter-container", wordResult);
    const div = $("div", "", con);
    if (count < hintLength) {
      if (isLetter() || hintLength - count == vocabulary.word.length - i) {
        div.text(letter);
        count++;
      }
    }
    div.style = `color: rgb(43, 54, 71);`;
    const underline = $("div", "underline", con);
    if (letter == " ") underline.style = `background-color: transparent;`;
  }
};
