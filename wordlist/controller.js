import { $ } from "../function/function.js";
import { localVocabularyStorage } from "../local-storage/local-storage.js";

export const renderWordList = (wordList, start, end, limitPerPage) => {
  if (wordList.length == 0) {
    const con = document.querySelector(".wordlist-container");
    con.innerHTML = "";
    const div = $("div", "wordlist-no-data-message", con);
    $("h3", "", div).text("No data in your word list");
    return;
  }
  const tbody = document.querySelector(".wordlist-tbody");
  tbody.innerHTML = "";
  const box = document.querySelector(".wordlist-confirm-box");
  const p = box.querySelector("p");
  const buttons = box.querySelectorAll("button");
  buttons[1].onclick = () => {
    box.style.display = "none";
  };

  for (let i = start; i < end; i++) {
    let vocab = wordList[i];

    let trClass = `${vocab.highlight ? " wordlist-highlightRow" : ""}`;
    const tr = $("tr", `wordlist-trTagTbody${trClass}`, tbody);
    $("td", "", tr).text(vocab.word);
    $("td", "", tr).text(vocab.translation);
    const highlight = $("td", "wordlist-highlightBtn", tr);
    $("i", "fa fa-check", highlight);
    const deleteBtn = $("td", "wordlist-deleteBtn", tr);
    $("i", "fa fa-times", deleteBtn);

    highlight.onclick = () => {
      p.innerText = "Are you sure you want to highlight this word?";
      buttons[0].onclick = () => {
        localVocabularyStorage.highlight(vocab);
        box.style.display = "none";
        let newWordList = localVocabularyStorage.get();
        renderPagination(1, newWordList, limitPerPage);
      };
      box.style.display = "block";
    };
    deleteBtn.onclick = () => {
      p.innerText = "Are you sure you want to delete this word?";
      buttons[0].onclick = () => {
        localVocabularyStorage.remove(vocab);
        box.style.display = "none";
        let newWordList = localVocabularyStorage.get();
        renderPagination(1, newWordList, limitPerPage);
      };
      box.style.display = "block";
    };
  }
};

export const renderPagination = (currentPage, wordList, limitPerPage) => {
  const row = document.querySelector(".pagination-row");
  row.innerHTML = "";
  let length = wordList.length;
  let totalPages = Math.ceil(length / limitPerPage);

  for (let i = 0; i < totalPages; i++) {
    let divClass = currentPage == i + 1 ? " pagination-active" : "";
    const item = $("div", `pagination-page-item${divClass}`, row).text(i + 1);

    item.onclick = () => {
      renderPagination(item.text(), wordList, limitPerPage);
    };
  }

  let start = (currentPage - 1) * limitPerPage;
  let end = start + limitPerPage > length ? length : start + limitPerPage;
  renderWordList(wordList, start, end, limitPerPage);
};
