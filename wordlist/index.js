import { hideLoading, showLoading } from "../function/function.js";
import { localVocabularyStorage } from "../local-storage/local-storage.js";
import { renderPagination } from "./controller.js";

const input = document.querySelector(".wordlist-search");
const limitPerPage = 10;

let wordList = [];

const fetchData = (search = "") => {
  showLoading();
  wordList = localVocabularyStorage.get(search);
  renderPagination(1, wordList, limitPerPage);
  hideLoading();
};

fetchData();

input.onkeyup = (e) => {
  if (e.key == "Enter") {
    let search = input.value.toLowerCase();
    fetchData(search);
  }
};
